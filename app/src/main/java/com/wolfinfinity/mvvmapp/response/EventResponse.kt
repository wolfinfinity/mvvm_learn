package com.wolfinfinity.mvvmapp.response

import com.google.gson.annotations.SerializedName
import com.wolfinfinity.mvvmapp.base.BaseResponse

data class EventResponse(

    @SerializedName("data")
    val data: ArrayList<EventData>

): BaseResponse() {
    data class EventData(

        @SerializedName("id")
        val id: String,

        @SerializedName("user_id")
        val userId: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("image")
        val image: String,

        @SerializedName("type")
        val type: String,

        @SerializedName("price")
        val price: String,

        @SerializedName("address")
        val address: String,

        @SerializedName("city")
        val city: String,

        @SerializedName("state")
        val state: String,

        @SerializedName("latitude")
        val latitude: String,

        @SerializedName("longitude")
        val longitude: String,

        @SerializedName("view")
        val view: String,

        @SerializedName("favorite_count")
        val favoriteCount: String,

        @SerializedName("start_date")
        val startDate: String,

        @SerializedName("end_date")
        val endDate: String,

        @SerializedName("start_time")
        val startTime: String,

        @SerializedName("end_time")
        val endTime: String,

        @SerializedName("favorite_status")
        val favoriteStatus: String,
    )
}
