package com.wolfinfinity.mvvmapp.ui.mainActivity

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wolfinfinity.mvvmapp.base.BaseViewModel
import com.wolfinfinity.mvvmapp.network.API_CONSTANTS
import com.wolfinfinity.mvvmapp.network.NetworkService
import com.wolfinfinity.mvvmapp.network.NetworkUtils
import com.wolfinfinity.mvvmapp.network.Resource
import com.wolfinfinity.mvvmapp.response.EventResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class MainViewModel @Inject constructor(
    var context: Context,
    var networkService: NetworkService,
    ) : BaseViewModel<MainNavigator>() {
    private val eventListObservable: MutableLiveData<Resource<EventResponse>> = MutableLiveData()

    fun getEventListObservable(): MutableLiveData<Resource<EventResponse>> {
        return eventListObservable
    }

    fun getEventList() {
        if (NetworkUtils.isNetworkConnected(context)) {
            var response: Response<EventResponse>
            val eventListDataRequest: MutableMap<String, String> = HashMap()

            eventListDataRequest[API_CONSTANTS.WEB_PARAM_PAGE] = "1"
            eventListDataRequest[API_CONSTANTS.WEB_PARAM_EVENT_TYPE] = ""
            eventListDataRequest[API_CONSTANTS.WEB_PARAM_FILTER_TYPE] = ""
            eventListDataRequest[API_CONSTANTS.WEB_PARAM_MY_ID] = "1"
            eventListDataRequest[API_CONSTANTS.WEB_PARAM_SEARCH_TEXT] = ""
            eventListDataRequest[API_CONSTANTS.WEB_PARAM_USER_ID] = ""

            viewModelScope.launch {
                eventListObservable.value = Resource.loading(null)
                withContext(Dispatchers.IO) {
                    response = networkService.eventList(eventListDataRequest)
                }

                withContext(Dispatchers.Main) {
                    response.run {
                        eventListObservable.value = baseDataSource.getResult(true) { this }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    eventListObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }
}