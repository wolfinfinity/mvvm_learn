package com.wolfinfinity.mvvmapp.ui.mainActivity

import android.os.Bundle
import android.util.Log
import android.view.View
import com.wolfinfinity.mvvmapp.BR
import com.wolfinfinity.mvvmapp.R
import com.wolfinfinity.mvvmapp.base.BaseActivity
import com.wolfinfinity.mvvmapp.databinding.ActivityMainBinding
import com.wolfinfinity.mvvmapp.network.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainNavigator,
    View.OnClickListener {

    override val layoutId: Int get() = R.layout.activity_main
    override val bindingVariable: Int get() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setNavigator(this)
        clickListener()
    }

    override fun setupObservable() {
        viewModel.getEventListObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    Log.w("MAIN_ACTIVITY", it.data.toString())
                }
                Resource.Status.LOADING -> {
                    Log.w("MAIN_ACTIVITY", it.data.toString())
                }
                Resource.Status.ERROR -> {
                    Log.w("MAIN_ACTIVITY", it.data.toString())
                }
                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.w("MAIN_ACTIVITY", it.data.toString())
                }
            }
        })
    }

    private fun clickListener() {
        binding.btGetList.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btGetList -> {
                viewModel.getEventList()
            }
        }
    }
}