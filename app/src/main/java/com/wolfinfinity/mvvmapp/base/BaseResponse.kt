package com.wolfinfinity.mvvmapp.base

import com.google.gson.annotations.SerializedName

open class BaseResponse(
    @SerializedName("status")
    var status: String = "",

    @SerializedName("message")
    var message: String = ""
)