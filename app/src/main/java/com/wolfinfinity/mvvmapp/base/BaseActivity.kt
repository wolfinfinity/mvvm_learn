package com.wolfinfinity.mvvmapp.base

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import javax.inject.Inject

abstract class BaseActivity<T: ViewDataBinding, V : ViewModel>: AppCompatActivity() {

    abstract val layoutId: Int
    abstract val bindingVariable: Int

    abstract fun setupObservable()

    @Inject
    lateinit var viewModel: V
    lateinit var binding: T

    @Inject
    lateinit var gson: Gson

    lateinit var activity: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        performDataBinding()
    }

    private fun performDataBinding() {
        binding = DataBindingUtil.setContentView(activity, layoutId)
        binding.setVariable(bindingVariable, viewModel)
        binding.executePendingBindings()
        setupObservable()
    }
}