package com.wolfinfinity.mvvmapp.base

import androidx.lifecycle.ViewModel
import java.lang.ref.WeakReference
import javax.inject.Inject

open class BaseViewModel<N>: ViewModel() {

    lateinit var navigator: WeakReference<N>

    fun getNavigator(): N? {
        return navigator.get()
    }

    fun setNavigator(navigator:N) {
        this.navigator= WeakReference(navigator)
    }

    @Inject
    lateinit var baseDataSource: BaseDataSource
}