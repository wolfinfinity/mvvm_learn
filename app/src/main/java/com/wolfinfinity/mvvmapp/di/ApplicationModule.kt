package com.wolfinfinity.mvvmapp.di

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.wolfinfinity.mvvmapp.base.BaseDataSource
import com.wolfinfinity.mvvmapp.network.API_CONSTANTS.Companion.BASE_URL
import com.wolfinfinity.mvvmapp.network.NetworkService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideContext(application: android.app.Application): Context {
        return application.applicationContext
    }

    @Provides
    fun provideNetworkService(retrofit: Retrofit): NetworkService =
        retrofit.create(NetworkService::class.java)

    @Provides
    @Singleton
    fun baseDataSources(): BaseDataSource {
        return BaseDataSource()
    }

    @Provides
    fun provideGson(): Gson = GsonBuilder().setLenient().create()

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson/*, userPreference: UserPreference*/): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient(/*userPreference*/))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    fun okHttpClient(/*userPreference: UserPreference*/): OkHttpClient {
        val levelType: HttpLoggingInterceptor.Level = if (androidx.viewbinding.BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        /*httpClient.addInterceptor(Interceptor { chain ->
            val originals: Request = chain.request()

            // Request customization: add request headers
            var apiKey: String = userPreference.getAPIToken()
            Log.e("TAG", "apikey=>$apiKey")

            val requestBuilder = originals.newBuilder()
                .header(HEADER_KEY, HEADER_VALUE)

            if (apiKey.isNotEmpty()) {
                requestBuilder.addHeader(X_API_KEY, apiKey)

            }
            val request = requestBuilder.build()
            chain.proceed(request)
        })*/

        httpClient.callTimeout(2, TimeUnit.MINUTES)
        httpClient.connectTimeout(2, TimeUnit.MINUTES)
        httpClient.readTimeout(2, TimeUnit.MINUTES)
        httpClient.writeTimeout(2, TimeUnit.MINUTES)
        httpClient.addNetworkInterceptor(logging)

        val client = httpClient.build()
        return client
    }
}