package com.wolfinfinity.mvvmapp.network

interface API_CONSTANTS {

    companion object {
        var BASE_URL = "http://intelligentscripts.com/eventapp/app/"

        const val WEB_API_EVENT_LIST = "event/geteventListing"

        const val WEB_PARAM_PAGE = "page"
        const val WEB_PARAM_USER_ID = "user_id"
        const val WEB_PARAM_MY_ID = "my_id"
        const val WEB_PARAM_FILTER_TYPE = "filter_type"
        const val WEB_PARAM_EVENT_TYPE = "event_type"
        const val WEB_PARAM_SEARCH_TEXT = "search_text"
    }
}