package com.wolfinfinity.mvvmapp.network

import com.wolfinfinity.mvvmapp.response.EventResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface NetworkService {

    /*@FormUrlEncoded
    @POST(API_CONSTANTS.WEB_API_EVENT_LIST)
    suspend fun eventList(@FieldMap paramLogin: Map<String, String>): Response<EventResponse>*/

    @POST(API_CONSTANTS.WEB_API_EVENT_LIST)
    suspend fun eventList(@Body hashMap: MutableMap<String, String>): Response<EventResponse>

}